#
# @brief Define tool or aperture
#
@def_tool = 0x1001
#
# @brief Change tool or aperture
#
@use_tool = 0x1002
#
# @brief Define aperture macro
#
@tool_macro = 0x1010

#
# @brief Interpolate operation
#
@int_op = 0x2001
#
# @brief Move operation
#
@move_op = 0x2002
#
# @brief Flash or drill operation
#
@flash_op = 0x2003

#
# @brief Linear interpolation mode
#
@lin_int = 0x2101
#
# @brief Clockwise interpolation mode
#
@cw_int = 0x2202
#
# @brief Counter Clockwise interpolation mode
#
@ccw_int = 0x2203

#
# @brief Region mode on
#
@reg_on = 0x10001
#
# @brief Region mode off
#
@reg_off = 0x10000

#
# @brief Dark or positive polarity
#
@dark_pol = 0x20001

#
# @brief Clear or negative polarity
#
@clear_pol = 0x20000

#
# @brief Single quadrant mode
#
@sin_quad = 0x30000

#
# @brief Multi quadrant mode
#
@mul_quad = 0x30001

#
# @brief Step and repeat mode on
#
@rep_on = 0x40001
#
# @brief Step and repeat mode off
#
@rep_off = 0x40002

#
# @brief Set attribute of file
#
@set_attr = 0x70000

#
# @brief Add attribute
#
@add_attr = 0x70001

#
# @brief Remove attribute
#
@del_attr = 0x70002

#
# @brief Comment block
#
@comment = 0x1

#
# @brief Circle shape
#
@circ_sh = 0x1

#
# @brief Line shape
#
@line_sh = 0x2

#
# @brief Rectangle shape
#
@rect_sh = 0x3

#
# @brief Outline shape
#
@olin_sh = 0x4

#
# @brief Oval shape
#
@oval_sh = 0x5

#
# @brief Polygon shape
#
@poly_sh = 0x6

#
# @brief Moire shape
#
@term_sh = 0x7

#
# @brief Moire shape
#
@moire_sh = 0x8

#
# @brief Macro shape
#
@macro_sh = 0x9
