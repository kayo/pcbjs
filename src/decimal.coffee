{
  digit
  oneOf
  string: str
  value: val
  sequence: seq
  or: Or
  and: And
  text
  bind
} = require "./mona-parser"

digit = do digit

@Decimal = class Decimal
  constructor: (@v = 0, @p = 0)->

  {min, pow} = Math

  toFloat: ->
    #@v * pow 10, @p
    ###
    v = @v
    p = @p
    while p > 0
      v *= 10
      p--
    while p < 0
      v /= 10
      p++
    v
    ###
    parseFloat @toString on
  
  toString: (e = no)->
    return "#{@v}e#{@p}" if e
    [s, v] = if @v < 0
      ["-", "#{-@v}"]
    else
      ["", "#{@v}"]
    if @p >= 0
      v = "#{v}#{(new Array @p+1).join "0"}"
    else if -@p >= v.length
      v = "0.#{(new Array 1-@p-v.length).join "0"}#{v}"
    else
      v = "#{v[...@p]}.#{v[@p...]}"
    s + v

  normalize: ->
    while @v % 1 isnt 0
      @v *= 10
      @p--
    while @v % 10 is 0
      @v = (@v / 10) | 0
      @p++
  
  @eq: (head, tail...)->
    return no for {v, p} in tail when head.v isnt v or head.p isnt p
    return yes

  sign = (a)-> if a > 0 then 1 else -1

  ge2 = (a, b)->
    sa = sign a.v
    sb = sign b.v
    return sa > sb if sa isnt sb
    (if sa > 0 then a.p > b.p else a.p < b.p) or (a.p is b.p and a.v >= b.v)
  
  @lt: (head, tail...)->
    return no for arg in tail when ge2 head, arg
    return yes

  @gt: (args...)->
    do args.reverse
    @lt args...

  @le: (args...)->
    not @gt args...

  @ge: (args...)->
    not @lt args...

  ###
  min_exp = (head, tail...)->
    min_p = head.p
    min_p = p for {p} in tail when p < min_p
    min_p

  to_exp = (min_p, args...)->
    v * pow 10, p - min_p for {v, p} in args
  
  apply_2 = (op, head, tail...)->
    head = op head, v for v in tail
    head
  
  ar_op = (op, po, args...)->
    min_p = min_exp args...
    args = to_exp min_p, args...
    result = new Decimal (apply_2 op, args...), (po min_p, args.length)
    do result.normalize
    result
  
  @add: (args...)->
    ar_op ((a, b)-> a + b), ((p)-> p), args...

  @sub: (args...)->
    ar_op ((a, b)-> a - b), ((p)-> p), args...

  @mul: (args...)->
    ar_op ((a, b)-> a * b), ((p, n)-> p * n), args...

  @div: (args...)->
    ar_op ((a, b)-> a / b), ((p, n)-> p * (2 - n)), args...
  ###
  
  min_exp = (a, b)->
    min a.p, b.p
  
  to_exp = (p, a)->
    a.v * pow 10, a.p - p

  ar_op2 = (f, e, a, b, t...)->
    p = min_exp a, b
    v = f (to_exp p, a), (to_exp p, b)
    r = new Decimal v, e p
    do r.normalize
    if t.length is 0
      r
    else
      ar_op2 f, e, r, t...
  
  @add: (args...)->
    ar_op2 ((a, b)-> a + b), ((p)-> p), args...
  
  @sub: (args...)->
    ar_op2 ((a, b)-> a - b), ((p)-> p), args...

  @mul: (args...)->
    ar_op2 ((a, b)-> a * b), ((p)-> p * 2), args...

  @div: (args...)->
    ar_op2 ((a, b)-> a / b), ((p)-> 0), args...

@decimal = seq ($)->
  s = $ Or (oneOf "-+"), val ""
  d = $ text digit
  f = $ Or (And (str "."), text digit), val ""
  d += f
  v = d.replace /(\d)0+$/, "$1"
  val new Decimal (parseInt s + v, 10), d.length - v.length - f.length

@decreal = bind @decimal, (d)-> val do d.toFloat
