{
  parse
  value: val
  or: Or
  and: And
  fail
  bind
  collect
  sequence: seq

  mapState
} = require "./mona-parser"

{Transform} = require "stream"
{StringDecoder} = require "string_decoder"

# @brief The internal parser state
#
class State
  # @brief Convert coordinate to number.
  #
  # This function handles numeric format and units.
  #
  # @param coord The specified coordinate ("x", "y")
  # @param sign  The sign of source value ("-", "+", "", null)
  # @param value The source value as string
  # @return The converted number
  #
  convCoord: (coord, sign, value, fract)-> # sign, value
    coord = do coord.toLowerCase
    if fract?
      value += ".#{fract}"
    else
      value = @_convFormat coord, value
    value = -value if sign is "-"
    @_convCoord coord, @_convUnits value

  # @brief Convert number to output units.
  #
  # @param value The source number
  # @return The result of conversion
  #
  convUnits: (value)->
    @_convUnits value

  getCoord: (coord, value)->
    if value?
      value
    else
      @_curCoords[coord]
  
  constructor: (@_parser, {units, zeros, coords, places})->
    @_curCoords =
      x: 0
      y: 0
    
    @_numPlaces =
      x: [2, 4]
      y: [2, 4]
    
    @_units = units or "in"
    
    @setUnits units
    @setZeros zeros
    @setCoords coords
    @setPlaces places

  convUnits =
    mm:
      mm: (v)-> v
      in: (v)-> v * (1 / 25.4)
    in:
      in: (v)-> v
      mm: (v)-> v * 25.4

  inch = /^i/i
  
  # @brief Set input units
  #
  # @param units The source units ("in", "mm")
  #
  setUnits: (units = @_units)->
    @_fileUnits = units? and (inch.test units) and "in" or "mm"
    @_convUnits = convUnits[@_fileUnits][@_units]

  placesCoord =
    x: "x"
    y: "y"
    i: "x"
    j: "y"
  
  convLeadZeros = (coord, value)->
    [n, m, a] = @_numPlaces[placesCoord[coord]]
    p = value.length - m # point place
    parseFloat if p < 0
      "0.#{(new Array 1-p).join "0"}#{value}"
    else
      "#{value[...p]}.#{value[p...]}"
    
  convTrailZeros = (coord, value)->
    [n, m, a] = @_numPlaces[coord]
    parseFloat if value.length < n
      "#{value}#{(new Array n - value.length).join "0"}"
    else
      "#{value[...n]}.#{value[n...]}"

  trail = /^t/i

  # @brief Set zeros omitting
  #
  # @param zeros The zeros omitted ("lead", "trail")
  #
  setZeros: (zeros)->
    @_convFormat = zeros? and (trail.test zeros) and convTrailZeros or convLeadZeros

  getZeros: -> @_convFormat is convTrailZeros and "trail" or "lead"

  convAbsCoords = (coord, value)->
    @_curCoords[coord] = value

  convIncCoords = (coord, value)->
    @_curCoords[coord] += value
  
  incremental = /^[ir]/i

  # @brief Set input coords
  #
  # @param coords The coordinates ("absolute", "incremental")
  #
  setCoords: (coords)->
    @_convCoord = coords? and (incremental.test coords) and convIncCoords or convAbsCoords
  
  # @brief Set coordinate format number places
  #
  # @param places The places by coord
  #
  setPlaces: (places)->
    return unless places?
    if places.length?
      @setPlaces x: places
      @setPlaces y: places
    else
      for coord, [n, m] of places
        coord = do coord.toLowerCase
        @_numPlaces[coord] = [n, m, n + m]
  
  getPlaces: -> @_numPlaces

  setFormat: (format)->
    @_parser.setFormat format

  pushCommand: (cmd)->
    if cmd?
      if cmd instanceof Array
        @_parser.push c for c in cmd
      else
        @_parser.push cmd

# @brief The streamed parser
#
@Parser = class Parser extends Transform
  wrapParser = (Command, emitCommand)->
    bind Command, (cmd)-> (state)->
      emitCommand cmd, state
  
  parsers = {}
  
  Tester = fail "No parsers"
  
  emitCommandTester = (format)-> (cmd, state)->
    (->
      @setFormat format
      @pushCommand cmd
    ).call state.userState
    state
  
  @register: (format, parser)->
    parsers[format] = parser
    Tester = Or (wrapParser p, emitCommandTester f for f, p of parsers)...
  
  _initTester: ->
    @_parser = Tester
  
  emitCommandParser = (cmd, state)->
    state.userState.pushCommand cmd
    state
  
  _initParser: ->
    @_parser = wrapParser parsers[@_format], emitCommandParser
  
  constructor: (opts = {})->
    @_format = opts.format if opts.format of parsers
    @_buffer = ""
    @_state = new State @, opts
    
    @_parserOpts =
      fileName: opts.fileName
      throwOnError: no
      returnState: yes
      allowTrailing: yes
      userState: @_state
    
    super readableObjectMode: on
    
    @_readableState?.objectMode = on
    @_decoder = new StringDecoder "utf8"
    
    if @_format
      do @_initParser
    else
      do @_initTester
  
  setFormat: (format)->
    unless @_format?
      @_format = format
      do @_initParser
  
  getFormat: -> @_format
  
  _iteration: ->
    state = parse @_parser, @_buffer, @_parserOpts
    if state instanceof Error
      unless state.wasEof
        state.buffer = @_buffer
        @emit "error", state
        @_buffer = null
      no
    else
      {position, input, offset} = state
      @_parserOpts.position = position
      @_buffer = if input? then input[offset...] else ""
      @_buffer.length > 0
  
  _iterations: ->
    while do @_iteration
      ;
  
  _transform: (chunk, encoding, done)->
    return done new Error "closed" unless @_buffer?
    @_buffer += @_decoder.write chunk
    do @_iterations
    do done
  
  _flush: (done)->
    return done new Error "closed" unless @_buffer?
    @_parserOpts.allowTrailing = no
    do @_iterations
    do done

@createParser = (opts)-> new Parser opts

coordConv = (coord)-> ([sign, value])-> @convCoord coord, sign, value

@convCoord = (coord, parser)-> mapState (coordConv coord), parser

valueConv = (value)-> @convUnits value

@convValue = (parser)-> mapState valueConv, parser
