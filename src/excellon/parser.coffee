{
  value: val
  string: str
  digit
  integer
  natural
  eof: Eof
  or: Or
  and: And
  not: Not
  maybe
  sequence: seq
  text
  collect
  oneOf
  noneOf
  bind
  splitEnd
  
  bindState
  mapState
  join
} = require "../mona-parser"

{
  def_tool
  use_tool
  tool_macro
  int_op
  move_op
  flash_op
  lin_int
  cw_int
  ccw_int
  reg_on
  reg_off
  dark_pol
  clear_pol
  sin_quad
  mul_quad
  rep_on
  rep_off
  set_attr
  add_attr
  del_attr
  comment
  finish
  circ_sh
  line_sh
  rect_sh
  olin_sh
  oval_sh
  poly_sh
  term_sh
  moire_sh
  macro_sh
} = require "../common"

{
  convCoord
  convValue
  Parser
} = require "../parser"

{decreal: dec} = require "../decimal"

ExpectCode = (c, n)-> bind (And (str c), nat), (v)->
  if v is n then val null else fail "#{c}#{n} expected"

eol = Or (str "\r\n"), (str "\n\r"), (str "\n"), (str "\r")

dig = digit 10
int = integer 10
nat = natural 10

makeFunction = ({G, D, M, X, Y, A, X1, Y1})->
  switch
    when G is 70
      @setUnits "in"
      null
    when G is 71
      @setUnits "mm"
      null
    when G is 90
      @setCoords "abs"
      null
    when G is 91
      @setCoords "inc"
      null
    when G is 0
      if X? or Y?
        c = $: move_op
        c.x = @getCoord "x", X if X?
        c.y = @getCoord "y", Y if Y?
        c
      else
        @_lastMode = move_op
        null
    when G is 1
      c = $: lin_int
      if X? or Y?
        c.x = @getCoord "x", X if X?
        c.y = @getCoord "y", Y if Y?
      else
        @_lastMode = int_op
      c
    when G is 2
      c = $: cw_int
      if X? or Y?
        c.x = @getCoord "x", X if X?
        c.y = @getCoord "y", Y if Y?
      else
        @_lastMode = int_op
      c
    when G is 3
      c = $: ccw_int
      if X? or Y?
        c.x = @getCoord "x", X if X?
        c.y = @getCoord "y", Y if Y?
      else
        @_lastMode = int_op
      c
    when G is 5
      if X? or Y?
        c.x = @getCoord "x", X if X?
        c.y = @getCoord "y", Y if Y?
        c
      else
        @_lastMode = flash_op
        null
    when G is 85
      [
        $: move_op
        x: @getCoord "x", X
        y: @getCoord "y", Y
      ,
        $: int_op
        m: lin_int
        x: @getCoord "x", X1
        y: @getCoord "y", Y1
      ]
    else
      if @_lastMode?
        c = $: @_lastMode
        c.x = @getCoord "x", X if X?
        c.y = @getCoord "y", Y if Y?
        c
      else
        null

CoordValue = join (Or (str "+"), (str "-"), val ""), (text dig), maybe (And (str "."), text dig)

ProgramCode = (o)-> bind (Or (oneOf "GDMXYA"), eol), (c)->
  switch c
    when "G", "D", "M"
      bind nat, (v)->
        if c of o
          fail "Multiple function codes #{c}"
        else
          o[c] = v
          ProgramCode o
    when "X", "Y", "A"
      bind (convCoord c, CoordValue), (v)->
        if c of o
          i = 1
          i++ while "#{c}#{i}" of o
          c = "#{c}#{i}"
        o[c] = v
        ProgramCode o
    else
      val o

ProgramCommand = mapState makeFunction, (args...)-> (ProgramCode {}) args...

ToolParam = seq ($)->
  switch $ oneOf "FBSCHZ"
    when "C"
      d = $ convValue dec
      val (o)->
        o.$ = def_tool
        o.d = d
    else
      $ dec
      val null

ToolCommand = seq ($)->
  t = $ And (str "T"), nat
  o = $: use_tool
  f o for f in $ collect Or ToolParam
  val o

SetUnits = And (mapState ((u)-> @setUnits u; null), Or (str "INCH,"), (str "METRIC,")),
  (mapState ((z)-> @setZeros z; null), Or (str "LZ"), (str "TZ"))

SkipHeaderRewind = And (str "%"), val null
SkipVersion = And (str "VER,"), nat, val null
SkipFormat = And (str "FMAT,"), nat, val null
SkipDetect = And (str "DETECT,"), (Or (str "ON"), (str "OFF")), val null

HeaderCommand = bind (Or SetUnits, SkipVersion, SkipFormat, SkipDetect, ToolCommand, SkipHeaderRewind), (o)-> And eol, val o

LineComment = And (Or (str ";"), (str "M47")), bind (text noneOf "\r\n"), (cmt)->
  And eol, val
    $: comment
    _: cmt

Command = Or LineComment, HeaderCommand, ProgramCommand

Parser.register "excellon", Command
