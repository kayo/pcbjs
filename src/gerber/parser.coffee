{
  value: val
  string: str
  between
  collect
  integer
  natural
  sequence: seq
  oneOf
  noneOf
  not: Not
  or: Or
  and: And
  exactly
  range
  maybe
  text
  alpha
  alphanum
  digit
  bind
  split
  fail

  bindState
  mapState
  join
} = require "../mona-parser"

{
  def_tool
  use_tool
  tool_macro
  
  int_op
  move_op
  flash_op
  
  lin_int
  cw_int
  ccw_int
  
  reg_on
  reg_off
  
  dark_pol
  clear_pol
  
  sin_quad
  mul_quad
  
  rep_on
  rep_off
  
  set_attr
  add_attr
  del_attr
  
  comment
  
  circ_sh
  line_sh
  rect_sh
  olin_sh
  oval_sh
  poly_sh
  term_sh
  moire_sh
  macro_sh
} = require "../common"

{
  convCoord
  convValue
  Parser
} = require "../parser"

{decreal: dec} = require "../decimal"

convDec = convValue dec

ExpectCode = (c, n)-> bind (And (str c), nat), (v)->
  if v is n then val null else fail "#{c}#{n} expected"

eol = Or (str "\r\n"), (str "\n\r"), (str "\n"), (str "\r")
eob = And (str "*"), (maybe eol) # the end of block
sep = And (str ","), (maybe eol) # the separator

tokX = str "X"
tokY = str "Y"
tokI = str "I"
tokJ = str "J"

dig = digit 10
int = integer 10
nat = natural 10

# [a-zA-Z_.$]{[a-zA-Z_.0-9]+}
Name = seq ($)->
  head = $ Or alpha(), oneOf "_.$"
  tail = $ text Or (alphanum 10), oneOf "_."
  val head + tail

StdName = seq ($)->
  $ str "."
  tail = $ text Or (alphanum 10), oneOf "_."
  val "." + tail

UserName = seq ($)->
  head = $ Or alpha(), oneOf "_$"
  tail = $ text Or (alphanum 10), oneOf "_."
  val head + tail

makeFunction = ({G, D, M, X, Y, I, J})->
  switch
    when G is 70
      @setUnits "in"
      null
    when G is 71
      @setUnits "mm"
      null
    when G is 90
      @setCoords "abs"
      null
    when G is 91
      @setCoords "inc"
      null
    when G is 36
      $: reg_on
    when G is 37
      $: reg_off
    when G is 74
      $: sin_quad
    when G is 75
      $: mul_quad
    when D >= 10
      $: use_tool
      t: D
    when D is 2
      c = $: move_op
      c.x = @getCoord "x", X if X?
      c.y = @getCoord "y", Y if Y?
      c
    when D is 3
      c = $: flash_op
      c.x = @getCoord "x", X if X?
      c.y = @getCoord "y", Y if Y?
      c
    when D is 1
      c = $: int_op
      c.x = @getCoord "x", X if X?
      c.y = @getCoord "y", Y if Y?
      c.i = @getCoord "i", I if I?
      c.j = @getCoord "j", J if J?
      switch
        when G is 1
          c.m = lin_int
        when G is 2
          c.m = cw_int
        when G is 3
          c.m = ccw_int
      c
    when G is 1
      $: lin_int
    when G is 2
      $: cw_int
    when G is 3
      $: ccw_int
    else
      null

CoordValue = join (Or (str "+"), (str "-"), val ""), text dig

FunctionCode = (o)-> bind (oneOf "GMXYIJD*"), (c)->
  switch c
    when "*"
      And (maybe eol), (val o)
    when "G", "D", "M"
      bind nat, (v)->
        if c of o
          fail "Multiple function codes #{c}"
        else
          o[c] = v
          FunctionCode o
    else
      bind (convCoord c, CoordValue), (v)->
        if c of o
          i = 1
          i++ while "#{c}#{i}" of o
          c = "#{c}#{i}"
        o[c] = v
        FunctionCode o

FunctionBlock = mapState makeFunction, (args...)-> (FunctionCode {}) args...

FSRange = join (range "0", "6"), (range "0", "6")

FSCommand = And (str "FS"),
  (mapState ((z)-> @setZeros z; null), oneOf "LTD"),
  (mapState ((c)-> @setCoords c; null), oneOf "AI"),
  (mapState (([n, m])-> @setPlaces x: [n|0, m|0]; null), And tokX, FSRange),
  (mapState (([n, m])-> @setPlaces y: [n|0, m|0]; null), And tokY, FSRange)

MOCommand = And (str "MO"),
  mapState ((u)-> @setUnits u; null), Or (str "IN"), (str "MM")

ADSep = tokX

ADC = seq ($)->
  $ str "C"
  diam = $ And sep, convDec
  hole = $ maybe And ADSep, convDec
  val (o)->
    o.s = circ_sh
    o.d = diam
    o.h = hole if hole?

ADRO = seq ($)->
  shape = $ oneOf "RO"
  xsize = $ And sep, convDec
  $ tokX
  ysize = $ convDec
  hole = $ maybe And ADSep, convDec
  val (o)->
    o.s = shape is "R" and rect_sh or oval_sh
    o.x = xsize
    o.y = ysize
    o.h = hole if hole?

ADP = seq ($)->
  $ str "P"
  diam = $ And sep, convDec
  $ tokX
  verts = $ convDec
  rotate = $ maybe And ADSep, convDec
  hole = $ maybe And ADSep, convDec
  val (o)->
    o.s = poly_sh
    o.d = diam
    o.v = verts
    o.r = rotate if rotate?
    o.h = hole if hole?

ADM = seq ($)->
  name = $ Name
  args = $ maybe And sep, split (convDec), ADSep
  val (o)->
    o.s = macro_sh
    o.n = name
    o.a = args if args?

ADCommand = seq ($)->
  $ str "ADD"
  o =
    $: def_tool
    t: $ nat
  ($ Or ADC, ADRO, ADP, ADM) o
  val o

AMExposure = And sep, Or (And (str "0"), (val off)), (And (str "1"), (val on))

AMRotation = And sep, dec

AMVariable = And (str "$"), nat

AsIs = (args...)-> args

AMUnaryOp = (op, term, app = (arg)-> [op, arg])->
  Or (bind (And (str op), term), (arg)-> val app arg), term

AMBinaryOp = (op, term, app = (args...)-> [op, args...])->
  #Or (bind (split term, (str op), min: 2), (args)-> val [op, args...]), term
  bind (split term, (str op), min: 1), (args)->
    val if args.length > 1 then app args... else args[0]

AMParens = between (str "("), (str ")"), (args...)->
  AMTerm0 args...

AMVarRef = bind AMVariable, (id)->
  val ["$", id]

AMTerm5 = Or AMParens, AMVarRef, convDec
AMTerm4 = AMUnaryOp "-", AMTerm5, (arg)->
  switch typeof arg
    when "number" then -arg
    else ["-", arg]
AMTerm3 = AMBinaryOp "/", AMTerm4
AMTerm2 = AMBinaryOp "x", AMTerm3
AMTerm1 = AMBinaryOp "-", AMTerm2
AMTerm0 = AMBinaryOp "+", AMTerm1

AMValue = AMTerm0

AMVarDef = seq ($)->
  id = $ AMVariable
  expr = $ And (str "="), AMValue
  val
    $: id
    _: expr

AMComment = And (str "0"), text (noneOf "*")

AMCircle = seq ($)->
  $ str "1"
  e = $ AMExposure
  d = $ And sep, AMValue
  x = $ And sep, AMValue
  y = $ And sep, AMValue
  r = $ Or AMRotation, val 0
  val {s: circ_sh, e, d, x, y, r}

AMLine = seq ($)->
  $ str "20"
  e = $ AMExposure
  w = $ And sep, AMValue
  x0 = $ And sep, AMValue
  y0 = $ And sep, AMValue
  x1 = $ And sep, AMValue
  y1 = $ And sep, AMValue
  r = $ AMRotation
  val {s: line_sh, e, w, x0, y0, x1, y1, r}

AMRect = seq ($)->
  $ str "21"
  e = $ AMExposure
  w = $ And sep, AMValue
  h = $ And sep, AMValue
  x = $ And sep, AMValue
  y = $ And sep, AMValue
  r = $ AMRotation
  val {s: rect_sh, e, w, h, x, y, r}

AMRectLL = seq ($)->
  $ str "22"
  e = $ AMExposure
  w = $ And sep, AMValue
  h = $ And sep, AMValue
  x = $ And sep, AMValue
  y = $ And sep, AMValue
  r = $ AMRotation
  val {s: "ll", e, w, h, x, y, r}

AMCoords = seq ($)-> # x,y pair
  x = $ AMValue
  y = $ And sep, AMValue
  val {x, y}

AMOutline = seq ($)->
  $ str "4"
  e = $ AMExposure
  n = $ And sep, nat
  p = $ exactly (And sep, AMCoords), n + 1
  r = $ AMRotation
  val {s: olin_sh, e, p, r}

AMPolygon = seq ($)->
  $ str "5"
  e = $ AMExposure
  n = $ And sep, nat
  x = $ And sep, AMValue
  y = $ And sep, AMValue
  d = $ And sep, AMValue
  r = $ AMRotation
  val {s: poly_sh, e, n, x, y, d, r}

AMMoire = seq ($)->
  $ str "6"
  x = $ And sep, AMValue
  y = $ And sep, AMValue
  d = $ And sep, AMValue
  t = $ And sep, AMValue
  g = $ And sep, AMValue
  n = $ And sep, AMValue
  c = $ And sep, AMValue
  l = $ And sep, AMValue
  r = $ AMRotation
  val {s: moire_sh, x, y, d, t, g, n, c, l, r}

AMThermal = seq ($)->
  $ str "7"
  x = $ And sep, AMValue
  y = $ And sep, AMValue
  d = $ And sep, AMValue
  h = $ And sep, AMValue
  g = $ And sep, AMValue
  r = $ AMRotation
  val {s: term_sh, x, y, d, h, g, r}

AMBlock = Or AMVarDef, AMComment, AMCircle, AMLine, AMRect, AMRectLL, AMOutline, AMPolygon, AMMoire, AMThermal

AMCommand = seq ($)->
  $ str "AM"
  name = $ Name
  defs = $ And eob, split AMBlock, eob
  val
    $: tool_macro
    n: name
    d: defs

SRMode = seq ($)->
  x = $ And tokX, nat
  y = $ And tokY, nat
  i = $ And tokI, convDec
  j = $ And tokJ, convDec
  val {$: rep_on, x, y, i, j}

SRCommand = And (str "SR"), Or SRMode, val $: rep_off

LPCommand = And (str "LP"), bind (oneOf "CD"), (pol)->
  val $: pol is "D" and dark_pol or clear_pol

DeprecatedIPCommand = And (str "IP"), bind (Or (str "POS"), (str "NEG")), (pol)-> val $: pol is "POS" and dark_pol or clear_pol

StdGerberAttrName = And (str "."), bind (Or (str "FileFunction"), (str "Part"), (str "MD5")), (name)-> val ".#{name}"

TFCommand = seq ($)->
  n = $ And (str "TF"), Or StdGerberAttrName, UserName
  f = $ Or (And sep, split (text noneOf ",*"), sep), val []
  val {$: set_attr, n, f}

StdApertureAttrName = And (str "."), bind (Or (str "AperFunction"), (str "DrillTolerance")), (name)-> val ".#{name}"

TACommand = seq ($)->
  n = $ And (str "TA"), Or StdApertureAttrName, UserName
  f = $ Or (And sep, split (text noneOf ",*"), sep), val []
  val {$: add_attr, n, f}

TDCommand = seq ($)->
  n = $ And (str "TD"), Or StdApertureAttrName, UserName
  val {$: del_attr, n}

ExtendedCode = Or FSCommand, MOCommand, ADCommand, AMCommand, SRCommand, LPCommand, TFCommand, TACommand, TDCommand, DeprecatedIPCommand

ExtendedBlock = between (str "%"), (And eob, (str "%"), maybe eol), ExtendedCode

CommentBlock = seq ($)->
  cmt = $ And (ExpectCode "G", 4), text noneOf "*"
  $ eob
  val
    $: comment
    _: cmt

Command = Or CommentBlock, ExtendedBlock, FunctionBlock

Parser.register "gerber", Command
