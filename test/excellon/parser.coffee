{deepEqual} = require "assert"
{createParser} = require "../../src/parser"
require "../../src/gerber/parser"
require "../../src/excellon/parser"
{
  def_tool
  use_tool
  tool_macro
  int_op
  move_op
  flash_op
  lin_int
  cw_int
  ccw_int
  reg_on
  reg_off
  dark_pol
  clear_pol
  sin_quad
  mul_quad
  rep_on
  rep_off
  set_attr
  add_attr
  del_attr
  comment
  finish
  circ_sh
  line_sh
  rect_sh
  olin_sh
  oval_sh
  poly_sh
  term_sh
  moire_sh
  macro_sh
} = require "../../src/common"
{info, pass, fail} = require "../common"
