{basename} = require "path"
{createReadStream} = require "fs"
{info, pass, fail} = require "./common"
{createParser} = require "../src/parser"
require "../src/gerber/parser"
require "../src/excellon/parser"

test = (fileNames, next)->
  return next? null if fileNames.length is 0
  
  [fileName, fileNames...] = fileNames
  
  info "File #{basename fileName}"
  
  p = createParser {fileName, units: "mm"}
  
  count = 0
  
  p.on "data", (command)->
  #  console.log "cmd -> ", command
    count += 1
  
  p.on "error", (error)->
    #console.error "err -> ", error
    fail error
    test fileNames, next
  
  p.on "end", ->
    pass "#{count} commands"
    test fileNames, next
  
  createReadStream fileName
  .pipe p

test ("test/sample/#{file}" for file in [
  "carte_test/carte_test-Back.gbl"
  "carte_test/carte_test-Front.gtl"
  "carte_test/carte_test-Mask_Front.gts"
  "carte_test/carte_test-SilkS_Back.gbo"
  "carte_test/carte_test-Mask_Back.gbs"
  "carte_test/carte_test-PCB Edge.gbr"
  "carte_test/carte_test-SilkS_Front.gto"
  "carte_test/carte_test.drl"
  "video/video-Composant.gtl"
  "video/video-Cuivre.gbl"
  "video/video-Interne_1.gbr"
  "video/video-Interne_2.gbr"
  "video/video-Mask_Back.gbs"
  "video/video-Mask_Front.gts"
  "video/video-PCB_Edge.gbr"
  "video/video-SilkS_Back.gbo"
  "video/video-SilkS_Front.gto"
  "video/video.drl"
])
