{parse} = require "mona-parser"
{Decimal, decimal} = require "../src/decimal"
{info, pass, fail} = require "./common"

tests = (test)->
  test "0", new Decimal 0, 0
  test "0.003", new Decimal 3, -3
  test "-0.001", new Decimal -1, -3
  test "1000", new Decimal 1, 3
  test "-210", new Decimal -21, 1
  test "123.456789", new Decimal 123456789, -6
  test "100.000009", new Decimal 100000009, -6
  test "-1230000000", new Decimal -123, 7
  test "0.00000123", new Decimal 123, -8
  test "0.0980", (new Decimal 98, -3), "0.098"
  test "9.80", (new Decimal 98, -1), "9.8"
  test "-0.70", (new Decimal -7, -1), "-0.7"

tests (value, expected)->
  info "Test parse #{value}"
  actual = parse decimal, value
  if Decimal.eq actual, expected
    do pass
  else
    fail "Expected #{expected.toString on} but actual is #{actual.toString on}!"

tests (expected, value, canonical)->
  info "Test stringify #{value.toString on}"
  actual = do value.toString
  expected = canonical if canonical?
  if actual is expected
    do pass
  else
    fail "Expected #{expected} but actual is #{actual}!"

tests = (test)->
  test "123.456", "add", "123", "0.456"
  test "0.122877", "add", "-0.000123000", "0.123"
  
  test "122.544", "sub", "123", "0.456"
  test "-0.123123", "sub", "-0.000123000", "0.123"
  
  test "56.088", "mul", "123", "0.456"
  test "-0.000015129", "mul", "-0.000123000", "0.123"
  
  test "269.7368421052631", "div", "123", "0.456"
  test "-0.001", "div", "-0.000123000", "0.123"
  
  test "1860.867", "mul", "123", "1.23", "12.3"
  test "823.8482384823848", "div", "456", "1.23", "0.45"

tests (expected, operator, values...)->
  expected = parse decimal, expected
  values = (parse decimal, value for value in values)
  info "Test operation #{operator} on values #{value.toString on for value in values}"
  actual = Decimal[operator] values...
  if Decimal.eq actual, expected
    do pass
  else
    fail "Expected #{expected.toString on} but actual is #{actual.toString on}!"

tests = (test)->
  test no, "eq", "1.23", "12.3"
  test yes, "eq", "1.23", "1.23"
  
  test yes, "lt", "1.23", "12.3"
  test no, "lt", "1.23", "1.23"
  
  test no, "gt", "1.23", "12.3"
  test no, "gt", "1.23", "1.23"

  test no, "ge", "1.23", "12.3"
  test yes, "ge", "1.23", "1.23"

  test yes, "le", "1.23", "12.3"
  test yes, "le", "1.23", "1.23"

  test no, "eq", "-0.001", "0.001"
  test yes, "eq", "-0.001", "-0.001"

  test yes, "eq", "-12.3", "-12.3"
  test no, "eq", "12.3", "-12.3"

  test no, "lt", "-1.23", "-12.3"
  test no, "lt", "-1.23", "-1.23"
  
  test yes, "gt", "-1.23", "-12.3"
  test no, "gt", "-1.23", "-1.23"

  test yes, "ge", "-1.23", "-12.3"
  test yes, "ge", "-1.23", "-1.23"

  test no, "le", "-1.23", "-12.3"
  test yes, "le", "-1.23", "-1.23"

tests (expected, operator, values...)->
  values = (parse decimal, value for value in values)
  info "Test operation #{operator} on values #{value.toString on for value in values}"
  actual = Decimal[operator] values...
  if actual is expected
    do pass
  else
    fail "Expected #{expected} but actual is #{actual}!"
