D = "\x1b[0m"
Q = "\x1b[1;37m"
R = "\x1b[1;31m"
G = "\x1b[1;32m"
I = "\x1b[1;34m"

print = (data)-> process.stderr.write data

ts = null

@info = (title)->
  str = "#{Q}.... ??#{D} #{I}#{title}#{D}"
  ts = do (new Date).getTime
  print str

@pass = (msg = "")->
  ts = do (new Date).getTime - ts
  print " #{R}[#{ts}ms]#{D} #{msg}\r#{G}PASS (:#{D}\n"

@fail = (msg)-> print "\r#{R}FAIL ):#{D}\n#{msg}\n"
