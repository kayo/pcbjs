{deepEqual} = require "assert"
{createParser} = require "../../src/parser"
require "../../src/gerber/parser"
require "../../src/excellon/parser"
{
  def_tool
  use_tool
  tool_macro
  int_op
  move_op
  flash_op
  lin_int
  cw_int
  ccw_int
  reg_on
  reg_off
  dark_pol
  clear_pol
  sin_quad
  mul_quad
  rep_on
  rep_off
  set_attr
  add_attr
  del_attr
  comment
  finish
  circ_sh
  line_sh
  rect_sh
  olin_sh
  oval_sh
  poly_sh
  term_sh
  moire_sh
  macro_sh
} = require "../../src/common"
{info, pass, fail} = require "../common"

#{startProfiling, stopProfiling} = require "v8-profiler"

process.on "exit", -> debugger

stringify = (v)->
  switch typeof v
    when "object"
      if v instanceof Array
        "[#{(stringify s for s in v).join ", "}]"
      else
        "{#{(k + ": " + stringify s for k, s of v).join ", "}}"
    when "boolean"
      if v then "on" else "off"
    else
      JSON.stringify v

test = (title, source, result)->
  #startProfiling title, on
  info "Test #{title}"
  p = createParser fileName: "#{title}.gbr"
  p.on "data", (command)->
    if result.length < 1
      fail "Too many commands: #{stringify command}"
      return
    [first, result...] = result
    ok = no
    try
      deepEqual first, command
      ok = yes
    unless ok
      fail """
        Parse error:
          Expected: #{stringify first}
          Actual: #{stringify command}
        """
  p.on "finish", ->
    if result.length > 0
      fail "Not enought commands: #{stringify result}"
    else
      do pass
  p.end source
  #stopProfiling title

test "Command Basic",
  """
  G04 Beginning of the file*
  %FSLAX25Y25*%
  %MOIN*%
  %LPD*%
  %ADD10C,0.000070*%
  X123500Y001250D02*
  X100Y125D1*
  X100Y125D01*
  X100Y125D0001*
  G002*
  G0000074*
  X100Y125D01*
  X100Y125D02*
  G01*
  G74*
  X0Y100I-400J100D01*
  X300Y200D02*
  X1100Y200D01*
  M02*
  """, [
    $: comment
    _: " Beginning of the file"
  ,
    $: dark_pol
  ,
    $: def_tool
    t: 10
    s: circ_sh
    d: 0.00007
  ,
    $: move_op
    x: 1.235
    y: 0.0125
  ,
    $: int_op
    x: 0.001
    y: 0.00125
  ,
    $: int_op
    x: 0.001
    y: 0.00125
  ,
    $: int_op
    x: 0.001
    y: 0.00125
  ,
    $: cw_int
  ,
    $: sin_quad
  ,
    $: int_op
    x: 0.001
    y: 0.00125
  ,
    $: move_op
    x: 0.001
    y: 0.00125
  ,
    $: lin_int
  ,
    $: sin_quad
  ,
    $: int_op
    x: 0
    y: 0.001
    i: -0.004
    j: 0.001
  ,
    $: move_op
    x: 0.003
    y: 0.002
  ,
    $: int_op
    x: 0.011
    y: 0.002
  ]

test "FS Command",
  """
  %FSLAX25Y25*%
  X-100Y125D01*
  X2300000Y-1250000D01*
  %FSLAX44Y34*%
  X1230Y-3210D01*
  X0Y0D01*
  X1Y-1D01*
  %FSTAX06Y06*%
  X100Y125D01*
  %FSLIX64Y64*%
  X100Y125D01*
  """, [
    $: int_op
    x: -0.001
    y: 0.00125
  ,
    $: int_op
    x: 23
    y: -12.5
  ,
    $: int_op
    x: 0.123
    y: -0.321
  ,
    $: int_op
    x: 0
    y: 0
  ,
    $: int_op
    x: 0.0001
    y: -0.0001
  ,
    $: int_op
    x: 0.1
    y: 0.125
  ,
    $: int_op
    x: 0.11
    y: 0.1375
  ]

test "MO Command",
  """
  %FSLAX34Y34*%
  %MOIN*%
  X41526Y789D01*
  %MOMM*%
  X41526Y789D01*
  """, [
    $: int_op
    x: 4.1526
    y: 0.0789
  ,
    $: int_op
    x: 0.16348818897637793
    y: 0.0031062992125984253
  ]

test "AD Command Basic",
  """
  %ADD10C,.025*%
  %ADD11C,0.5X0.25*%
  %ADD12C,1*%
  %ADD22R,.050X.050X.027*%
  %ADD22R,0.044X0.025*%
  %ADD22R,0.044X0.025X0.019*%
  %ADD57O,.030X.040X.015*%
  %ADD22O,0.046X0.026*%
  %ADD22O,0.046X0.026X0.019*%
  %ADD30P,.016X6*%
  %ADD17P,.040X6*%
  %ADD17P,.040X6X0.0X0.019*%
  """, [
    $: def_tool
    t: 10
    s: circ_sh
    d: 0.025
  ,
    $: def_tool
    t: 11
    s: circ_sh
    d: 0.5
    h: 0.25
  ,
    $: def_tool
    t: 12
    s: circ_sh
    d: 1
  ,
    $: def_tool
    t: 22
    s: rect_sh
    x: 0.05
    y: 0.05
    h: 0.027
  ,
    $: def_tool
    t: 22
    s: rect_sh
    x: 0.044
    y: 0.025
  ,
    $: def_tool
    t: 22
    s: rect_sh
    x: 0.044
    y: 0.025
    h: 0.019
  ,
    $: def_tool
    t: 57
    s: oval_sh
    x: 0.03
    y: 0.04
    h: 0.015
  ,
    $: def_tool
    t: 22
    s: oval_sh
    x: 0.046
    y: 0.026
  ,
    $: def_tool
    t: 22
    s: oval_sh
    x: 0.046
    y: 0.026
    h: 0.019
  ,
    $: def_tool
    t: 30
    s: poly_sh
    d: 0.016
    v: 6
  ,
    $: def_tool
    t: 17
    s: poly_sh
    d: 0.04
    v: 6
  ,
    $: def_tool
    t: 17
    s: poly_sh
    d: 0.04
    v: 6
    r: 0.0
    h: 0.019
  ]

test "AD Command Macro",
  """
  %ADD15CIRC*%
  %ADD10SQUAREWITHHOLE*%
  %ADD146RECT,0.0807087X0.1023622X0.0118110X0.5000000X0.3000000*%
  %ADD30DONUTVAR,0.100X0X0X0.080*%
  %ADD37TARGET,0.020*%
  %ADD51REC1,0.02X0.01*%
  %ADD52REC2,0.02X0.01*%
  %ADD33DONUTFIX*%
  %ADD34DONUTVAR,0.100X0X0X0.080*%
  %ADD35DONUTCAL,0.020X0X0*%
  """, [
    $: def_tool
    t: 15
    s: macro_sh
    n: "CIRC"
  ,
    $: def_tool
    t: 10,
    s: macro_sh
    n: "SQUAREWITHHOLE"
  ,
    $: def_tool
    t: 146
    s: macro_sh,
    n: "RECT"
    a: [0.0807087, 0.1023622, 0.0118110, 0.5000000, 0.3000000]
  ,
    $: def_tool
    t: 30
    s: macro_sh
    n: "DONUTVAR"
    a: [0.100, 0, 0, 0.080]
  ,
    $: def_tool
    t: 37
    s: macro_sh
    n: "TARGET"
    a: [0.020]
  ,
    $: def_tool
    t: 51
    s: macro_sh
    n: "REC1"
    a: [0.02, 0.01]
  ,
    $: def_tool
    t: 52
    s: macro_sh
    n: "REC2"
    a: [0.02, 0.01]
  ,
    $: def_tool
    t: 33
    s: macro_sh
    n: "DONUTFIX"
  ,
    $: def_tool
    t: 34
    s: macro_sh
    n: "DONUTVAR"
    a: [0.100, 0, 0, 0.080]
  ,
    $: def_tool
    t: 35
    s: macro_sh
    n: "DONUTCAL"
    a: [0.020, 0, 0]
  ]

test "AM Command Basic",
  """
  %AMCIRCLE*
  1,1,1.5,0,0,0*%
  %AMDONUTFIX*
  1,1,0.100,0,0*
  1,0,0.080,0,0*%
  %AMLINE*
  20,1,0.9,0,0.45,12,0.45,0*%
  %AMRECTANGLE*
  21,1,6.8,1.2,3.4,0.6,30*%
  %AMSQUAREWITHHOLE*
  21,1,10,10,0,0,0*
  1,0,5,0,0*%
  %AMOUTLINE*
  4,1,4,
  0.1,0.1,
  0.5,0.1,
  0.5,0.5,
  0.1,0.5,
  0.1,0.1,
  0*%
  %AMTRIANGLE_30*
  4,1,3,1,-1,1,1,2,1,1,-1,30*%
  %AMPOLYGON*
  5,1,8,0,0,8,0*%
  %AMMOIRE*
  6,0,0,5,0.5,0.5,2,0.1,6,0*%
  """, [
    $: tool_macro
    n: "CIRCLE"
    d: [ s: circ_sh, e: on, d: 1.5, x: 0, y: 0, r: 0 ]
  ,
    $: tool_macro
    n: "DONUTFIX"
    d: [
      s: circ_sh, e: on, d: 0.1, x: 0, y: 0, r: 0
    ,
      s: circ_sh, e: off, d: 0.08, x: 0, y: 0, r: 0
    ]
  ,
    $: tool_macro
    n: "LINE"
    d: [ s: line_sh, e: on, w: 0.9, x0: 0, y0: 0.45, x1: 12, y1: 0.45, r: 0 ]
  ,
    $: tool_macro
    n: "RECTANGLE"
    d: [ s: rect_sh, e: on, w: 6.8, h: 1.2, x: 3.4, y: 0.6, r: 30 ]
  ,
    $: tool_macro
    n: "SQUAREWITHHOLE"
    d: [
      s: rect_sh, e: on, w: 10, h: 10, x: 0, y: 0, r: 0
    ,
      s: circ_sh, e: off, d: 5, x: 0, y: 0, r: 0
    ]
  ,
    $: tool_macro
    n: "OUTLINE"
    d: [ s: olin_sh, e: on, p: [
      x: 0.1, y: 0.1
    ,
      x: 0.5, y: 0.1
    ,
      x: 0.5, y: 0.5
    ,
      x: 0.1, y: 0.5
    ,
      x: 0.1, y: 0.1
    ], r: 0 ]
  ,
    $: tool_macro
    n: "TRIANGLE_30"
    d: [ s: olin_sh, e: on, p: [
      x: 1, y: -1
    ,
      x: 1, y: 1
    ,
      x: 2, y: 1
    ,
      x: 1, y: -1
    ], r: 30 ]
  ,
    $: tool_macro
    n: "POLYGON"
    d: [ s: poly_sh, e: on, n: 8, x: 0, y: 0, d: 8, r: 0 ]
  ,
    $: tool_macro
    n: "MOIRE"
    d: [ s: moire_sh, x: 0, y: 0, d: 5, t: 0.5, g: 0.5, n: 2, c: 0.1, l: 6, r: 0 ]
  ]

test "AM Command Extra",
  """
  %AMDONUTVAR*1,1,$1,$2,$3*1,0,$4,$2,$3*%
  %AMDONUTVAR*
  1,1,$1,$2,$3*
  1,0,$4,$2,$3*%
  %AMRECT*
  21,1,$1,$2-$3-$3,-$4,-$5,0*%
  %AMDONUTCAL*
  1,1,$1,$2,$3*
  $4=$1x1.25*
  1,0,$4,$2,$3*%
  %AMREC1*
  $2=$1*
  $1=$2*
  21,1,$1,$2,0,0,0*%
  %AMREC2*
  $1=$2*
  $2=$1*
  21,1,$1,$2,0,0,0*%
  %AMTEST*
  1,1,$1,$2,$3*
  $4=$1x0.75*
  $5=($2+100)x1.75*
  1,0,$4,$5,$3*%
  """, [
    $: tool_macro
    n: "DONUTVAR"
    d: [
      s: circ_sh, e: on, d: ["$", 1], x: ["$", 2], y: ["$", 3], r: 0
    ,
      s: circ_sh, e: off, d: ["$", 4], x: ["$", 2], y: ["$", 3], r: 0
    ]
  ,
    $: tool_macro
    n: "DONUTVAR"
    d: [
      s: circ_sh, e: on, d: ["$", 1], x: ["$", 2], y: ["$", 3], r: 0
    ,
      s: circ_sh, e: off, d: ["$", 4], x: ["$", 2], y: ["$", 3], r: 0
    ]
  ,
    $: tool_macro
    n: "RECT"
    d: [ s: rect_sh, e: on, w: ["$", 1], h: ["-", ["$", 2], ["$", 3], ["$", 3]], x: ["-", ["$", 4]], y: ["-", ["$", 5]], r: 0 ]
  ,
    $: tool_macro
    n: "DONUTCAL"
    d: [
      s: circ_sh, e: on, d: ["$", 1], x: ["$", 2], y: ["$", 3], r: 0
    ,
      $: 4, _: ["x", ["$", 1], 1.25]
    ,
      s: circ_sh, e: off, d: ["$", 4], x: ["$", 2], y: ["$", 3], r: 0
    ]
  ,
    $: tool_macro
    n: "REC1"
    d: [
      $: 2, _: ["$", 1]
    ,
      $: 1, _: ["$", 2]
    ,
      s: rect_sh, e: on, w: ["$", 1], h: ["$", 2], x: 0, y: 0, r: 0
    ]
  ,
    $: tool_macro
    n: "REC2"
    d: [
      $: 1, _: ["$", 2]
    ,
      $: 2, _: ["$", 1]
    ,
      s: rect_sh, e: on, w: ["$", 1], h: ["$", 2], x: 0, y: 0, r: 0
    ]
  ,
    $: tool_macro
    n: "TEST"
    d: [
      s: circ_sh, e: on, d: ["$", 1], x: ["$", 2], y: ["$", 3], r: 0
    ,
      $: 4, _: ["x", ["$", 1], 0.75]
    ,
      $: 5, _: ["x", ["+", ["$", 2], 100], 1.75]
    ,
      s: circ_sh, e: off, d: ["$", 4], x: ["$", 5], y: ["$", 3], r: 0
    ]
  ]

test "AM Command Heavy",
  """
  %AMTEST*
  $4=$1x0.75*
  $5=100+$3*
  1,1,$1,$2,$3*
  1,0,$4,$2,$5*
  $6=$4x0.5*
  1,0,$6,$2,$5*%
  %AMTARGET*
  1,1,$1,0,0*
  $1=$1x0.8*
  1,0,$1,0,0*
  $1=$1x0.8*
  1,1,$1,0,0*
  $1=$1x0.8*
  1,0,$1,0,0*
  $1=$1x0.8*
  1,1,$1,0,0*
  $1=$1x0.8*
  1,0,$1,0,0*%
  %AMRECTROUNDCORNERS*
  21,1,$1,$2-$3-$3,-$4,-$5,0*
  $9=$1/2*
  $8=$2/2*
  22,1,$1-$3-$3,$3,-$9+$3-$4,$8-$3-$5,0*
  22,1,$1-$3-$3,$3,-$9+$3-$4,-$8-$5,0*
  1,1,$3+$3,-$4+$9-$3,-$5+$8-$3*
  1,1,$3+$3,-$4-$9+$3,-$5+$8-$3*
  1,1,$3+$3,-$4-$9+$3,-$5-$8+$3*
  1,1,$3+$3,-$4+$9-$3,-$5-$8+$3*%
  %AMRECTROUNDCORNERS*
  0 Rectangle with rounded corners. *
  0 Offsets $4 and $5 are interpreted as the *
  0 offset of the flash origin from the pad center. *
  0 First create horizontal rectangle. *
  21,1,$1,$2-$3-$3,-$4,-$5,0*
  0 From now on, use width and height half-sizes. *
  $9=$1/2*
  $8=$2/2*
  0 Add top and bottom rectangles. *
  22,1,$1-$3-$3,$3,-$9+$3-$4,$8-$3-$5,0*
  22,1,$1-$3-$3,$3,-$9+$3-$4,-$8-$5,0*
  0 Add circles at the corners. *
  1,1,$3+$3,-$4+$9-$3,-$5+$8-$3*
  1,1,$3+$3,-$4-$9+$3,-$5+$8-$3*
  1,1,$3+$3,-$4-$9+$3,-$5-$8+$3*
  1,1,$3+$3,-$4+$9-$3,-$5-$8+$3*%
  """, [
    $: tool_macro
    n: "TEST"
    d: [
      $: 4, _: ["x", ["$", 1], 0.75]
    ,
      $: 5, _: ["+", 100, ["$", 3]]
    ,
      s: circ_sh, e: on, d: ["$", 1], x: ["$", 2], y: ["$", 3], r: 0
    ,
      s: circ_sh, e: off, d: ["$", 4], x: ["$", 2], y: ["$", 5], r: 0
    ,
      $: 6, _: ["x", ["$", 4], 0.5]
    ,
      s: circ_sh, e: off, d: ["$", 6], x: ["$", 2], y: ["$", 5], r: 0
    ]
  ,
    $: tool_macro
    n: "TARGET"
    d: [
      s: circ_sh, e: on, d: ["$", 1], x: 0, y: 0, r: 0
    ,
      $: 1, _: ["x", ["$", 1], 0.8]
    ,
      s: circ_sh, e: off, d: ["$", 1], x: 0, y: 0, r: 0
    ,
      $: 1, _: ["x", ["$", 1], 0.8]
    ,
      s: circ_sh, e: on, d: ["$", 1], x: 0, y: 0, r: 0
    ,
      $: 1, _: ["x", ["$", 1], 0.8]
    ,
      s: circ_sh, e: off, d: ["$", 1], x: 0, y: 0, r: 0
    ,
      $: 1, _: ["x", ["$", 1], 0.8]
    ,
      s: circ_sh, e: on, d: ["$", 1], x: 0, y: 0, r: 0
    ,
      $: 1, _: ["x", ["$", 1], 0.8]
    ,
      s: circ_sh, e: off, d: ["$", 1], x: 0, y: 0, r: 0
    ]
  ,
    $: tool_macro
    n: "RECTROUNDCORNERS"
    d: [
      s: rect_sh, e: on, w: ["$", 1], h: ["-", ["$", 2], ["$", 3], ["$", 3]], x: ["-", ["$", 4]], y: ["-", ["$", 5]], r: 0
    ,
      $: 9, _: ["/", ["$", 1], 2]
    ,
      $: 8, _: ["/", ["$", 2], 2]
    ,
      s: "ll", e: on, w: ["-", ["$", 1], ["$", 3], ["$", 3]], h: ["$", 3], x: ["+", ["-", ["$", 9]], ["-", ["$", 3], ["$", 4]]], y: ["-", ["$", 8], ["$", 3], ["$", 5]], r: 0
    ,
      s: "ll", e: on, w: ["-", ["$", 1], ["$", 3], ["$", 3]], h: ["$", 3], x: ["+", ["-", ["$", 9]], ["-", ["$", 3], ["$", 4]]], y: ["-", ["-", ["$", 8]], ["$", 5]], r: 0
    ,
      s: circ_sh, e: on, d: ["+", ["$", 3], ["$", 3]], x: ["+", ["-", ["$", 4]], ["-", ["$", 9], ["$", 3]]], y: ["+", ["-", ["$", 5]], ["-", ["$", 8], ["$", 3]]], r: 0
    ,
      s: circ_sh, e: on, d: ["+", ["$", 3], ["$", 3]], x: ["+", ["-", ["-", ["$", 4]], ["$", 9]], ["$", 3]], y: ["+", ["-", ["$", 5]], ["-", ["$", 8], ["$", 3]]], r: 0
    ,
      s: circ_sh, e: on, d: ["+", ["$", 3], ["$", 3]], x: ["+", ["-", ["-", ["$", 4]], ["$", 9]], ["$", 3]], y: ["+", ["-", ["-", ["$", 5]], ["$", 8]], ["$", 3]], r: 0
    ,
      s: circ_sh, e: on, d: ["+", ["$", 3], ["$", 3]], x: ["+", ["-", ["$", 4]], ["-", ["$", 9], ["$", 3]]], y: ["+", ["-", ["-", ["$", 5]], ["$", 8]], ["$", 3]], r: 0
    ]
  ,
    $: tool_macro
    n: "RECTROUNDCORNERS"
    d: [
      " Rectangle with rounded corners. "
    ,
      " Offsets $4 and $5 are interpreted as the "
    ,
      " offset of the flash origin from the pad center. "
    ,
      " First create horizontal rectangle. "
    ,
      s: rect_sh, e: on, w: ["$", 1], h: ["-", ["$", 2], ["$", 3], ["$", 3]], x: ["-", ["$", 4]], y: ["-", ["$", 5]], r: 0
    ,
      " From now on, use width and height half-sizes. "
    ,
      $: 9, _: ["/", ["$", 1], 2]
    ,
      $: 8, _: ["/", ["$", 2], 2]
    ,
      " Add top and bottom rectangles. "
    ,
      s: "ll", e: on, w: ["-", ["$", 1], ["$", 3], ["$", 3]], h: ["$", 3], x: ["+", ["-", ["$", 9]], ["-", ["$", 3], ["$", 4]]], y: ["-", ["$", 8], ["$", 3], ["$", 5]], r: 0
    ,
      s: "ll", e: on, w: ["-", ["$", 1], ["$", 3], ["$", 3]], h: ["$", 3], x: ["+", ["-", ["$", 9]], ["-", ["$", 3], ["$", 4]]], y: ["-", ["-", ["$", 8]], ["$", 5]], r: 0
    ,
      " Add circles at the corners. "
    ,
      s: circ_sh, e: on, d: ["+", ["$", 3], ["$", 3]], x: ["+", ["-", ["$", 4]], ["-", ["$", 9], ["$", 3]]], y: ["+", ["-", ["$", 5]], ["-", ["$", 8], ["$", 3]]], r: 0
    ,
      s: circ_sh, e: on, d: ["+", ["$", 3], ["$", 3]], x: ["+", ["-", ["-", ["$", 4]], ["$", 9]], ["$", 3]], y: ["+", ["-", ["$", 5]], ["-", ["$", 8], ["$", 3]]], r: 0
    ,
      s: circ_sh, e: on, d: ["+", ["$", 3], ["$", 3]], x: ["+", ["-", ["-", ["$", 4]], ["$", 9]], ["$", 3]], y: ["+", ["-", ["-", ["$", 5]], ["$", 8]], ["$", 3]], r: 0
    ,
      s: circ_sh, e: on, d: ["+", ["$", 3], ["$", 3]], x: ["+", ["-", ["$", 4]], ["-", ["$", 9], ["$", 3]]], y: ["+", ["-", ["-", ["$", 5]], ["$", 8]], ["$", 3]], r: 0
    ]
  ]

test "SR command",
  """
  %SRX3Y2I5.0J4.0*%
  %SRX2Y3I2.0J3.0*%
  %SRX4Y1I5.0J0*%
  %SRX1Y1I0J0*%
  %SR*%
  """, [
    $: rep_on
    x: 3
    y: 2
    i: 5.0
    j: 4.0
  ,
    $: rep_on
    x: 2
    y: 3
    i: 2.0
    j: 3.0
  ,
    $: rep_on
    x: 4
    y: 1
    i: 5.0
    j: 0.0
  ,
    $: rep_on
    x: 1
    y: 1
    i: 0
    j: 0
  ,
    $: rep_off
  ]

test "LP Command",
  """
  %LPD*%
  %LPC*%
  """, [
    $: dark_pol
  ,
    $: clear_pol
  ]

test "TF Command",
  """
  %TF.FileFunction,Soldermask,Top*%
  %TFMyApertureAttributeWithValue,value*%
  %TFMyApertureAttributeWithoutValue*%
  """, [
    $: set_attr
    n: ".FileFunction"
    f: [ "Soldermask", "Top" ]
  ,
    $: set_attr
    n: "MyApertureAttributeWithValue"
    f: [ "value" ]
  ,
    $: set_attr
    n: "MyApertureAttributeWithoutValue"
    f: []
  ]

test "TA Command",
  """
  %TA.AperFunction,ComponentPad*%
  %TAMyApertureAttributeWithValue,value*%
  %TAMyApertureAttributeWithoutValue*%
  %TA.AperFunction,ComponentPad*%
  %TA.AperFunction,ViaPad*%
  """, [
    $: add_attr
    n: ".AperFunction"
    f: [ "ComponentPad" ]
  ,
    $: add_attr
    n: "MyApertureAttributeWithValue"
    f: [ "value" ]
  ,
    $: add_attr
    n: "MyApertureAttributeWithoutValue"
    f: []
  ,
    $: add_attr
    n: ".AperFunction"
    f: [ "ComponentPad" ]
  ,
    $: add_attr
    n: ".AperFunction"
    f: [ "ViaPad" ]
  ]

test "TD Command",
  """
  %TD.AperFunction*%
  %TDMyApertureAttributeWithValue*%
  %TDMyApertureAttributeWithoutValue*%
  """, [
    $: del_attr
    n: ".AperFunction"
  ,
    $: del_attr
    n: "MyApertureAttributeWithValue"
  ,
    $: del_attr
    n: "MyApertureAttributeWithoutValue"
  ]
